import { Server as SocketServer } from 'socket.io';
import { Inject } from '../core/injector';
import { CoincapService } from '../services/coincap';
import { Job } from './_base';

export class CryptocurrencyListener extends Job {
    public jobId = 'cryptocurrency-listener';
    @Inject() private coincapService: CoincapService;
    @Inject() private socket: SocketServer;

    public async execute() {
        const assets = await this.coincapService.getAssets();
        this.socket.emit('crypto.assetsUpdated', assets);
    }
}
