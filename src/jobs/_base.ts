import * as Agenda from 'agenda';
import * as logger from 'winston';

export abstract class Job {
    public abstract jobId: string;
    protected job: Agenda.Job;

    public abstract async execute(): Promise<void>;

    public invoke(jobExecutor: Agenda) {
        jobExecutor.define(this.jobId, async (job: Agenda.Job, done: any) => {
            this.job = job;
            logger.info(`${this.jobId}: executing job`);

            try {
                await this.execute();
                logger.info(`Job '${this.jobId}' executed`);
                done();
            }
            catch (err) {
                logger.error(`Job '${this.jobId}' of type ${this.constructor.name} failed to execute`, err);
                done(err);
            }
        });

        return this;
    }

    public async touch() {
        return this.job.touch();
    }
}
