import * as config from 'config';
import * as express from 'express';
import { Server } from 'http';
import * as path from 'path';
import * as socketIO from 'socket.io';
import { injector } from './core/injector';
import { executor } from './core/job-executor';
import { CryptocurrencyListener } from './jobs/cryptocurrency-listener';
import { CoincapService } from './services/coincap';

const namespace = config.get('app.apiNamespace');
const port = config.get('app.port');
const app = express();

app.use('/public', express.static(__dirname + '/webapp'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'webapp', 'index.html'));
});

app.get(`${namespace}/assets`, async function (req, res) {
    const coincapService = new CoincapService();
    res.json(await coincapService.getAssets());
});

const server = new Server(app);
const socket = socketIO(server);
server.listen(port);

// Make socket injectable through the custom DI
injector.set('socket', socket);

executor.then((agenda) => {
    const cryptocurrencyListener = new CryptocurrencyListener().invoke(agenda);
    agenda.every('5 seconds', cryptocurrencyListener.jobId);

    agenda.start();
});
