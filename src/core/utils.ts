export function toLowerFirst(target: string){
    return target && target[0].toLowerCase() + target.slice(1);
}
