/// Aliases for commonly used interfaces in TypeScript

export interface Dictionary<T = any> {
    [key: string]: T;
}

export type Constructable<T> = new(...args: Array<any>) => T;
