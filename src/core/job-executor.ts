/// Initialization of Agenda (cron) instance

import * as Agenda from 'agenda';
import * as config from 'config';
import * as logger from 'winston';

const mongoUrl = config.get('database.url') as string;
const mongoName = config.get('database.name') as string;
const mongoOptions = config.get('database.options');

logger.info('creating job executor instance');

export const executor = new Promise((resolve: (agenda: Agenda) => void, reject) => {
    const agenda = new Agenda(
        {
            db: {
                address: `${mongoUrl}/${mongoName}`,
                options: mongoOptions,
            },
        },
        (error) => {
            if (error) {
                logger.error('EXECUTOR_INITIALIZATION_FAILED', error);
                reject(error);
            }
        }
    ).on('ready', () => {
        logger.info('job executor is ready');
        resolve(agenda);
    });

    agenda.on('error', (e) => {
        logger.error('EXECUTOR_WRONG_EVENT', e);
    });
});
