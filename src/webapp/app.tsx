import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as socketIO from 'socket.io';
import { CurrencyList } from './component';

function renderList(currencies: Array<any>) {
    ReactDOM.render(
        <CurrencyList currencies={currencies} />,
        document.getElementById('currency-list')
    );
}

(async () => {
    const socket = socketIO('http://localhost:3005');

    socket.on('crypto.assetsUpdated', (data: any) => {
        renderList(data);
    });

    const response = await fetch('http://localhost:3005/api/assets');
    renderList(await response.json());

})();
