import * as React from 'react';

export interface CurrencyListProps {
    currencies: Array<any>;
}

export class CurrencyList extends React.Component<CurrencyListProps> {
    public render() {
        return <React.Fragment>
            <h4 className='d-flex justify-content-between align-items-center mb-3'>
                <span className='text-muted'>Top rated currencies</span>
                <span className='badge badge-secondary badge-pill'>{this.props.currencies.length}</span>
            </h4>
            <ul className='list-group mb-3'>
                {this.props.currencies.map(
                    (item, i) =>
                        <li key={i} className={'list-group-item d-flex justify-content-between lh-condensed'}>
                            <div>
                                <h6 className='my-0'>{item.name}</h6>
                                <small className='text-muted'>{item.symbol}</small>
                            </div>
                            <span className='text-muted'>{item.priceUsd}</span>
                        </li>
                )}
            </ul>
        </React.Fragment>;
    }
}
