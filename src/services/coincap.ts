import * as request from 'request-promise';
import { Injectable } from '../core/injector';

export interface AssetResponse {
    data: Array<CryptocurrencyAsset>;
    timestamp: number;
}

export interface CryptocurrencyAsset {
    id: string;
    rank: number;
    symbol: string;
    name: string;
    priceUsd: string;
}

@Injectable()
export class CoincapService {
    public async getAssets(limit = 5): Promise<Array<Partial<CryptocurrencyAsset>>> {
        const body = await request('https://api.coincap.io/v2/assets');
        const { data } = JSON.parse(body) as AssetResponse;
        
        /// If we are not sure that API will always return "data" array sorted by rank
        return data
            .sort(
                (left, right) => Number(left.rank) - Number(right.rank)
            )
            .slice(0, 5)
            .map(
                (item) => ({
                    id: item.id,
                    name: item.name,
                    symbol: item.symbol,
                    rank: Number(item.rank),
                    priceUsd: item.priceUsd,
                })
            );
    }
}
