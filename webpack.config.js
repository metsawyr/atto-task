const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './src/webapp/app.tsx',
  resolve: {
    extensions: ['.ts', '.tsx']
  },
  output: {
    path: path.join(__dirname, '/dist/webapp'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      { 
        test: /\.tsx?$/, 
        loader: 'awesome-typescript-loader'
      }
    ]
  },
  externals: [
    {
      "react": "React",
      "react-dom": "ReactDOM",
      "socket.io": "io",
    }
  ],
  plugins: [
    new CopyWebpackPlugin([{
      from: './src/webapp/index.html'
    }])
  ]
};
