### Cryptocurrency observer
Basic implementation of cryptocurrency aggregator over single external API. 

### Implementation details
WebSocket connection is established by Socker.io library, so reconnection is implemented on it's side. Web application is made using React. Jobs are running over Agenda (which requires MongoDB in order to run tasks correctly).

### Required software
- Node.js. Developed on v10.14.2 but should be supported on all 9th versions
- NPM
- MongoDB or Docker 

### Steps to run
- Clone this reposigtory usign `git clone`
- Install dependencies `npm install`
- Build an application `npm run build`
- Run MongoDB. If you have docker, perform `docker pull mongo` and `docker run --name mongodb-container -p 27017:27017 -d mongo:latest`
- Run an application `npm start`

Application is available at `http://localhost:3005`